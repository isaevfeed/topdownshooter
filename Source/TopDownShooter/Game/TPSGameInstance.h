// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "TPSGameInstance.generated.h"

struct FWeaponInfo;

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTPSGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	UDataTable* WeaponInfoTable;

	FWeaponInfo* FindWeaponDataByName(FName WeaponName);
};

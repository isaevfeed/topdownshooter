// Fill out your copyright notice in the Description page of Project Settings.


#include "../Game/TPSGameInstance.h"
#include "../FuncLibrary/Types.h"

FWeaponInfo* UTPSGameInstance::FindWeaponDataByName(FName WeaponName)
{
	auto WeaponInfo = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponName, "", false);

	if (!WeaponInfo) {
		return nullptr;
	}

	return WeaponInfo;
}

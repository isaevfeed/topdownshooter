// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

#include "../Components/WeaponComponent.h"

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	WeaponComponent = CreateDefaultSubobject<UWeaponComponent>("WeaponComponent");

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}*/

	MovementTick(DeltaSeconds);
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Aim", EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::AimEnable);
	PlayerInputComponent->BindAction("Aim", EInputEvent::IE_Released, this, &ATopDownShooterCharacter::AimDisable);
	PlayerInputComponent->BindAction("MakeShot", EInputEvent::IE_Pressed, WeaponComponent, &UWeaponComponent::StartFire);
	PlayerInputComponent->BindAction("MakeShot", EInputEvent::IE_Released, WeaponComponent, &UWeaponComponent::StopFire);
	PlayerInputComponent->BindAction("Reload", EInputEvent::IE_Pressed, WeaponComponent, &UWeaponComponent::Reload);
	PlayerInputComponent->BindAction("RunForward", EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::RunStart);
	PlayerInputComponent->BindAction("RunForward", EInputEvent::IE_Released, this, &ATopDownShooterCharacter::RunEnd);
	PlayerInputComponent->BindAxis("MoveForward", this, &ATopDownShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATopDownShooterCharacter::MoveRight);
}

void ATopDownShooterCharacter::MoveForward(float Value)
{
	AxisX = Value;
}

void ATopDownShooterCharacter::MoveRight(float Value)
{
	if (IsRunning) return;

	AxisY = Value;
}

void ATopDownShooterCharacter::RunStart()
{
	IsRunning = true;
}

void ATopDownShooterCharacter::RunEnd()
{
	IsRunning = false;
}

void ATopDownShooterCharacter::AimEnable()
{
	IsAim = true;
}

void ATopDownShooterCharacter::AimDisable()
{
	IsAim = false;
}

void ATopDownShooterCharacter::MovementTick(float DeltaSeconds)
{
	AddMovementInput(FVector(1.0, 0, 0), AxisX);
	AddMovementInput(FVector(0, 1.0, 0), AxisY);

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (!MyController) return;

	FHitResult HitResult;
	MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, false, HitResult);

	FRotator CursorRot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location);

	SetActorRotation(FQuat(FRotator(0, CursorRot.Yaw, 0)));
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float NewSpeed = MovementInfo.RunSpeed;

	switch(MovementState) {
	case EMovementState::Aim_State:
		NewSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		NewSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		NewSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		NewSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		NewSpeed = MovementInfo.RunSpeed;
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
}

void ATopDownShooterCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "../FuncLibrary/Types.h"

#include "BaseWeapon.generated.h"

class UNiagaraComponent;

UCLASS()
class TOPDOWNSHOOTER_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FName WeaponName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	class USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FName MuzzleSocketName = "MuzzleFlash";

	UPROPERTY(BlueprintReadWrite)
	bool Reloading = false;

	// Sets default values for this actor's properties
	ABaseWeapon();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void MakeShot();

	virtual void StartFire();
	virtual void StopFire();
	void Reload();
	void EndReload();

protected:
	FTimerHandle TimerHandler;
	FWeaponInfo* WeaponSettings;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MakeTraceShot();
	void MakeLineTrace(FHitResult& HitResult, FVector& TraceStart, FVector& TraceEnd);
	void SetDirection(FVector& TraceStart, FVector& TraceEnd);

	void SpawnMuzzleFX(UNiagaraSystem* NiagaraFX);
	void SpawnTraceFX(FVector TraceStart, FVector TraceEnd);
	void SpawnWeaponSound(USoundBase* Sound);
	void InitWeaponSettings();

	void DegreesAmmo();
	bool IsAmmoEmpty() const;
	bool IsBulletsEmpty() const;
	bool IsClipsEmpty() const;
};

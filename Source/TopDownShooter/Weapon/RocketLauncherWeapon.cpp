// Fill out your copyright notice in the Description page of Project Settings.


#include "../Weapon/RocketLauncherWeapon.h"
#include "../Character/TopDownShooterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "../Weapon/BaseProjectile.h"

void ARocketLauncherWeapon::StartFire()
{
	if (IsAmmoEmpty()) {
		SpawnWeaponSound(WeaponSettings->SoundLow);

		return;
	}

	SpawnWeaponSound(WeaponSettings->SoundFire);
	SpawnProjectile();
	DegreesAmmo();
}

void ARocketLauncherWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void ARocketLauncherWeapon::SpawnProjectile()
{
	ACharacter* Char = Cast<ACharacter>(GetOwner());

	if (!Char) {
		return;
	}

	FVector TraceStart, TraceEnd;
	SetDirection(TraceStart, TraceEnd);

	FTransform SpawnTransform(Char->GetActorRotation(), TraceStart);
	ABaseProjectile* Projectile = Cast<ABaseProjectile>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ProjectileClass, SpawnTransform));

	if (Projectile) {
		Projectile->SetShotDirection((TraceEnd - TraceStart).GetSafeNormal());

		UGameplayStatics::FinishSpawningActor(Projectile, SpawnTransform);
	}
}

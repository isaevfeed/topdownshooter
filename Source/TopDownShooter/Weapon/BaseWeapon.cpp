// Fill out your copyright notice in the Description page of Project Settings.

#include "../Weapon/BaseWeapon.h"

#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"

#include "../Game/TPSGameInstance.h"
#include "../Character/TopDownShooterCharacter.h"

// Sets default values
ABaseWeapon::ABaseWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	SetRootComponent(SkeletalMesh);
}

// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	InitWeaponSettings();
}

void ABaseWeapon::MakeShot()
{
	MakeTraceShot();
}

void ABaseWeapon::MakeTraceShot()
{
	if (IsAmmoEmpty()) {
		SpawnWeaponSound(WeaponSettings->SoundLow);
	}

	if (Reloading || IsAmmoEmpty() || IsBulletsEmpty()) {
		return;
	}

	FVector TraceStart;
	FVector TraceEnd;
	FHitResult HitResult;
	MakeLineTrace(HitResult, TraceStart, TraceEnd);

	if (HitResult.bBlockingHit) {
		//UGameplayStatics::SpawnDecalAtLocation(HitResult.Location);

		TraceEnd = HitResult.ImpactPoint;
	}

	SpawnMuzzleFX(WeaponSettings->ShotFX);
	SpawnTraceFX(TraceStart, TraceEnd);
	SpawnWeaponSound(WeaponSettings->SoundFire);

	DegreesAmmo();
}

void ABaseWeapon::MakeLineTrace(FHitResult& HitResult, FVector& TraceStart, FVector& TraceEnd)
{
	SetDirection(TraceStart, TraceEnd);

	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility);
}

void ABaseWeapon::SetDirection(FVector& TraceStart, FVector& TraceEnd)
{
	FTransform MuzzleTransform = SkeletalMesh->GetSocketTransform(MuzzleSocketName);
	TraceStart = MuzzleTransform.GetLocation();
	FVector ShotDirection = FMath::VRandCone(MuzzleTransform.GetRotation().GetForwardVector(), WeaponSettings->DispertionRadius / 100) * WeaponSettings->TraceDistance;
	TraceEnd = MuzzleTransform.GetLocation() + ShotDirection;
}

void ABaseWeapon::SpawnMuzzleFX(UNiagaraSystem* NiagaraFX)
{
	UNiagaraFunctionLibrary::SpawnSystemAttached(
		NiagaraFX,
		SkeletalMesh,
		MuzzleSocketName,
		FVector::ZeroVector,
		FRotator::ZeroRotator,
		EAttachLocation::SnapToTarget,
		true
	);
}

void ABaseWeapon::SpawnTraceFX(FVector TraceStart, FVector TraceEnd)
{
	const auto TraceFX = UNiagaraFunctionLibrary::SpawnSystemAtLocation(
		GetWorld(),
		WeaponSettings->TraceFX,
		TraceStart
	);

	if (TraceFX) {
		TraceFX->SetNiagaraVariableVec3(TEXT("TraceEnd"), TraceEnd);
	}
}

void ABaseWeapon::SpawnWeaponSound(USoundBase* Sound)
{
	UGameplayStatics::SpawnSoundAtLocation(
		this,
		Sound,
		GetActorLocation()
	);
}

void ABaseWeapon::InitWeaponSettings()
{
	UTPSGameInstance* GmInst = Cast<UTPSGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	WeaponSettings = GmInst->FindWeaponDataByName(WeaponName);

	WeaponSettings->WeaponAmmo.Bullets = WeaponSettings->WeaponAmmo.MaxBullets;
	WeaponSettings->WeaponAmmo.Clips = WeaponSettings->WeaponAmmo.MaxClips;
}

void ABaseWeapon::DegreesAmmo()
{
	WeaponSettings->WeaponAmmo.Bullets -= 1;
	WeaponSettings->WeaponAmmo.Bullets = FMath::Clamp(WeaponSettings->WeaponAmmo.Bullets, 0, WeaponSettings->WeaponAmmo.MaxBullets);

	if (WeaponSettings->WeaponAmmo.Bullets == 0 && WeaponSettings->WeaponAmmo.Clips > 0) {
		Reload();
	}
}

bool ABaseWeapon::IsAmmoEmpty() const
{
	return WeaponSettings->WeaponAmmo.Bullets == 0 && WeaponSettings->WeaponAmmo.Clips == 0;
}

bool ABaseWeapon::IsBulletsEmpty() const
{
	return WeaponSettings->WeaponAmmo.Bullets == 0;
}

bool ABaseWeapon::IsClipsEmpty() const
{
	return WeaponSettings->WeaponAmmo.Clips == 0;
}

// Called every frame
void ABaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseWeapon::StartFire()
{
	GetWorld()->GetTimerManager().SetTimer(
		TimerHandler,
		this, 
		&ABaseWeapon::MakeShot, 
		WeaponSettings->RateOfFire,
		true,
		0
	);
}

void ABaseWeapon::StopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandler);
}

void ABaseWeapon::Reload()
{
	if (IsAmmoEmpty() || IsClipsEmpty()) {
		return;
	}

	Reloading = true;

	ATopDownShooterCharacter* Character = Cast<ATopDownShooterCharacter>(GetOwner());

	if (!Character) {
		Reloading = true;

		return;
	}

	Character->PlayAnimMontage(WeaponSettings->AnimReaload);

	WeaponSettings->WeaponAmmo.Bullets = WeaponSettings->WeaponAmmo.MaxBullets;
	WeaponSettings->WeaponAmmo.Clips -= 1;
	WeaponSettings->WeaponAmmo.Clips = FMath::Clamp(WeaponSettings->WeaponAmmo.Clips, 0, WeaponSettings->WeaponAmmo.MaxClips);
}

void ABaseWeapon::EndReload()
{
	Reloading = false;
}


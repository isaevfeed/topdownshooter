// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Weapon/BaseProjectile.h"
#include "../Weapon/BaseWeapon.h"
#include "RocketLauncherWeapon.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ARocketLauncherWeapon : public ABaseWeapon
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
	TSubclassOf<ABaseProjectile> ProjectileClass;

	virtual void StartFire() override;

protected:
	virtual void BeginPlay() override;

	void SpawnProjectile();
};

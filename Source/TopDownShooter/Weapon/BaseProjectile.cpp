// Fill out your copyright notice in the Description page of Project Settings.


#include "../Weapon/BaseProjectile.h"
#include "../Weapon/BaseWeapon.h"
#include "../Character/TopDownShooterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
ABaseProjectile::ABaseProjectile()
{
	PrimaryActorTick.bCanEverTick = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("MovementComponent");
}

// Called when the game starts or when spawned
void ABaseProjectile::BeginPlay()
{
	Super::BeginPlay();

	MovementComponent->Velocity = ShotDirection * MovementComponent->InitialSpeed;

	StaticMesh->OnComponentHit.AddDynamic(this, &ABaseProjectile::OverlapHandle);
}

void ABaseProjectile::OverlapHandle(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Error, TEXT("SHOOT"));
	if (OtherActor) {
		if (Cast<ABaseWeapon>(OtherActor) || Cast<ATopDownShooterCharacter>(OtherActor)) {
			return;
		}
		else {
			UE_LOG(LogTemp, Error, TEXT("SHOOT"));
		}

		UNiagaraFunctionLibrary::SpawnSystemAtLocation(
			GetWorld(),
			DestroyFX,
			GetActorLocation(),
			FRotator::ZeroRotator
		);

		UGameplayStatics::SpawnSoundAtLocation(
			this,
			SoundExplosion,
			GetActorLocation(),
			FRotator::ZeroRotator,
			1.0F,
			1.0F,
			0.7F
		);

		Destroy();
	}
}


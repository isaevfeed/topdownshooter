// Fill out your copyright notice in the Description page of Project Settings.

#include "../Components/WeaponComponent.h"

#include "Kismet/GameplayStatics.h"

#include "../Weapon/BaseWeapon.h"
#include "../Character/TopDownShooterCharacter.h"
#include "../Game/TPSGameInstance.h"

// Sets default values for this component's properties
UWeaponComponent::UWeaponComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	SpawnWeapon();
}

void UWeaponComponent::SpawnWeapon()
{
	if (!GetWorld()) {
		return;
	}

	auto Character = Cast<ATopDownShooterCharacter>(GetOwner());

	if (!Character) {
		return;
	}

	CurrentWeapon = GetWorld()->SpawnActor<ABaseWeapon>(WeaponClass);

	if (!CurrentWeapon) {
		return;
	}

	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
	CurrentWeapon->AttachToComponent(Character->GetMesh(), AttachmentRules, WeaponSocketName);
	CurrentWeapon->SetOwner(GetOwner());
}

void UWeaponComponent::StartFire()
{
	if (!CurrentWeapon) {
		return;
	}

	CurrentWeapon->StartFire();
}

void UWeaponComponent::StopFire()
{
	if (!CurrentWeapon) {
		return;
	}

	CurrentWeapon->StopFire();
}

void UWeaponComponent::Reload()
{
	CurrentWeapon->Reload();
}

void UWeaponComponent::EndReload()
{
	CurrentWeapon->EndReload();
}

